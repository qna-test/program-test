## QUESTION
Buatlah koding dengan menggunakan bahasa pemrograman yang dikuasai sehingga menghasilkan
output sebagai berikut:

```text
Input : 7
Output :
7000000
600000
50000
4000
300
20
1
```

## ANSWER

---
```java
package org.program;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input : ");
        int input = scanner.nextInt();
        scanner.close();
        String result = generateOutput(input);
        System.out.println(result);
    }

    public static String generateOutput(int input) {
        StringBuilder output = new StringBuilder();
        for (int i = input; i >= 1; i--) {
            output.append(i);
            if (i > 1) {
                output.append("0".repeat(i - 1));
            }
            output.append("\n");
        }
        return output.toString();
    }
}
```
---
## RESULT

![RESULT](result/result.png)

![RESULT2](result/result2.png)