package org.program;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input : ");
        int input = scanner.nextInt();
        scanner.close();
        String result = generateOutput(input);
        System.out.println(result);
    }

    public static String generateOutput(int input) {
        StringBuilder output = new StringBuilder();
        for (int i = input; i >= 1; i--) {
            output.append(i);
            if (i > 1) {
                output.append("0".repeat(i - 1));
            }
            output.append("\n");
        }
        return output.toString();
    }
}